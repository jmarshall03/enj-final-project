#include <sqlite3.h>
#include <switch>
#include <stdio.h>
#include <utility>
#include "string.h"
using namespace std;

bool isAdmin = false;

/**
 *  Returns an uppercase version of the string passed
 *
 *  @param str: the string to return in all caps
 *
 *  @return an all caps string
 */
std::string touppercase(std::string str) {
	transform(str.begin(), str.end(), str.begin(), ::toupper);
	return str;
}
/**
 *  Returns a lowercase version of the string passed
 *
 *  @param str: the string to return lowercase
 *
 *  @return a lowecase string
 */
std::string tolowercase(std::string str) {
	transform(str.begin(), str.end(), str.begin(), ::tolower);
	return str;
}
/**
 *  Returns a capitalized version of the string passed
 *
 *  @param str: the string to return capitalized
 *
 *  @return a capitalized string
 */
std::string capitalize(std::string str){
	str = tolowercase(str);
	str[0] = toupper(str[0]);
	return str;
}

/**
 *  Returns a list of menu options for admin users
 *
 *  @return a string list of options
 */
string adminMenu(){
	string res = "";
	cout << "\nOption 1: View all orders (1)\n";
	cout << "Option 2: View all meals (2)\n";
	cout << "Option 3: Create new meals (3)\n";
	cout << "Option 4: Create new entrees (4)\n";
	cout << "Option 5: Create new sides (5)\n";
	cout << "Option 6: Delete Meals (6)\n";
	cout << "Option 7: Delete Entrees (7)\n";
	cout << "Option 8: Delete Sides (8)\n";
	cout << "Option 9: View Customer's Orders(Must have customer's id)(0):\n";
	cout << "Option 10: Update Entrees (10)\n";
	cout << "Option 11: Update Sides (11)\n";
	cout << "Option 12: View all customers (12)\n";
	cout << "Option 13: View A Customer (13)\n";
	cout << "Exit: (exit)\n";
	cout << "\nSelect an option #:";
	cin >> res;

	return res;
}

/**
 *  Returns a list of menu options for users who are customer
 *
 *  @return a string list of options
 */
string customerMenu(){
	string res;

	cout << "\nWelcome customer, what would you like to do?\n";
	cout << "Option 1: View Menu\n";
	cout << "Exit: (exit)\n";
	cout << "\nOnline Ordering Will Be Available Soon!\n";
	cout << "\nSelect an option #:";
	cin >> res;

	return res;
}

/**
 *  Returns information about a customer
 *  	you can get this information by only entering the customers id
 *  	if you dont know the id, pass 0 as the id, and then pass their name
 *
 *  @param int id: customer id
 *  @param char fname: first name of the customer
 *  @param char lname: last name of the customer
 *
 *  @return prints out customer id, first name, last name, phone, and address if user exists
 */
void getCustomer(int id ,char *fname = "joe", char *lname = "smoe"){
	sqlite3 *db;
	int rc;
	char sqlStr[256];

	rc = sqlite3_open("MealPrepBizFinal.db", &db);

	if(rc){
		//Output if db connection was unsuccessful
		fprintf(stderr, "Can't open database: %s\n", sqlite3_errmsg(db));
	} else{
		//Prepare Select query to retrieve customer information from DB
		//If searching by customer id
		if(id != 0){
			sqlite3_stmt *stmt;
			rc = sqlite3_prepare_v2(db, "SELECT * FROM CUSTOMERS WHERE CUSTOMER_ID = ?1;", -1, &stmt, NULL);
			if( rc == SQLITE_OK ) {
				//Bind id var value to query
				sqlite3_bind_int(stmt, 1, id);
			}
			while ( (rc = sqlite3_step(stmt)) == SQLITE_ROW) {
				//output query results formatted as a table
				printf("\n%-15s%-15s%-20s%-20s%-20s\n","Customer ID", "First Name", "Last Name", "Phone", "Address");
				printf("-----------------------------------------------------------------------------1------------------\n");
				printf("%-15d%-15s%-20s%-20s%-20s\n", sqlite3_column_int(stmt, 0), sqlite3_column_text(stmt, 1),sqlite3_column_text(stmt, 2),sqlite3_column_text(stmt, 3),sqlite3_column_text(stmt, 4));
			}
			//Close query
			sqlite3_finalize(stmt);

		} else{
			//If searching by first and last name
			sqlite3_stmt *stmt;

			sprintf(sqlStr,"SELECT * FROM CUSTOMERS WHERE CUSTOMER_ID WHERE UPPER(`FNAME`) = '%s' AND UPPER(`LNAME`) = '%s';", fname, lname);
			cout << "\n" << sqlStr << "\n";
			sqlite3_prepare_v2(db,sqlStr, -1, &stmt, NULL);

			while ( (rc = sqlite3_step(stmt)) == SQLITE_ROW) {
				printf("\n%-15s%-15s%-20s%-20s%-20s\n","Customer ID", "First Name", "Last Name", "Phone", "Address");
				printf("%-15d%-15s%-20s%-20s%-20s\n", sqlite3_column_int(stmt, 0), sqlite3_column_text(stmt, 1),sqlite3_column_text(stmt, 2),sqlite3_column_text(stmt, 3),sqlite3_column_text(stmt, 4));
			}
			//Close query
			sqlite3_finalize(stmt);
		}
	}
	//close db connection
	sqlite3_close(db);
}

/**
 *  Returns order information about a specific customer
 *
 *  @param int id: customer id
 *
 *  @return prints out customer id, first name, last name, phone, and address if user exists
 */
void getCustomersOrders(int id){
	sqlite3 *db;
	int rc;
	rc = sqlite3_open("MealPrepBizFinal.db", &db);

	//Output whether or db connection was successful
	if(rc){
		fprintf(stderr, "Can't open database: %s\n", sqlite3_errmsg(db));
	} else{
		//Prepare Select query to retrieve customer information from DB
		if(id != 0){
			sqlite3_stmt *stmt;
			rc = sqlite3_prepare_v2(db, "SELECT C.FNAME, C.LNAME, M.ENTREE, M.SIDE1,M.SIDE2, M.PRICE, P.DATE_ORDERED, P.AVAIL_PICKUP "
										"FROM MEALS M, CUSTOMERS C, PROCESSED P "
										"WHERE C.CUSTOMER_ID = ?1 "
										"AND C.CUSTOMER_ID = P.CUSTOMER_ID "
										"AND M.ID = P.MEAL_ID;", -1, &stmt, NULL);
			if( rc == SQLITE_OK ) {
				sqlite3_bind_int(stmt, 1, id);
			}
			//Prints out all order info in table format
			printf("\n%-15s%-15s%-35s%-20s%-20s%-10s%-20s%-20s\n","First Name", "Last Name", "Entree","Side 1", "Side 2", "Price", "Order Date", "Avail For Pickup");
			while ( (rc = sqlite3_step(stmt)) == SQLITE_ROW) {                                              /* 2 */
				printf("----------------------------------------------------------------------------------------------------------------------------------------------------------------\n");
				printf("%-15s%-15s%-35s%-20s%-20s$%-10s%-20s%-20s\n", sqlite3_column_text(stmt, 0), sqlite3_column_text(stmt, 1),sqlite3_column_text(stmt, 2),
						sqlite3_column_text(stmt, 3),sqlite3_column_text(stmt, 4),sqlite3_column_text(stmt, 5),sqlite3_column_text(stmt, 6),sqlite3_column_text(stmt, 7));
			}
			//Close query
			sqlite3_finalize(stmt);
		}
	}
	//close db connection
	sqlite3_close(db);
}

/**
 *  Returns a list of meals
 */
void getMeals(){
	sqlite3 *db;
	int rc;
	//Open connection to database
	rc = sqlite3_open("MealPrepBizFinal.db", &db);

	//Output whether or db connection was successful
	if(rc){
		fprintf(stderr, "Can't open database: %s\n", sqlite3_errmsg(db));
	} else{
		//Prepare Select query to retrieve customer information from DB
		sqlite3_stmt *stmt;
		sqlite3_prepare_v2(db, "SELECT * FROM MEALS;", -1, &stmt, NULL);

		//Get Query Results
		//Loop Through query results and output each row
		printf("\n%-10s%-35s%-25s%-25s%-10s\n", "Option #", "Entree", "Side 1", "Side 2", "Price");
		while ( (rc = sqlite3_step(stmt)) == SQLITE_ROW) {                                              /* 2 */
			printf("---------------------------------------------------------------------------------------------------\n");
			printf("%-10d%-35s%-25s%-25s$%-10s\n",sqlite3_column_int(stmt, 0), sqlite3_column_text(stmt, 3),sqlite3_column_text(stmt, 1),sqlite3_column_text(stmt, 2),sqlite3_column_text(stmt, 4));
		}
		//Close query
		sqlite3_finalize(stmt);
	}
	//close db connection
	sqlite3_close(db);
}

/**
 *  Returns the user type
 *
 *  @return if user is admin or customer in string format
 */
string getUser() {
	string user = "";
	string adminpwd = "adminpass";
	string res = "";

	cout << "Are you a customer or admin? (customer or admin): ";
	cin >> user;

	user = touppercase(user);
	SWITCH (user)
	CASE("ADMIN")

		while(res != "exit"){
			cout << "Admin Password: ";
			cin >> res;
			if(res == adminpwd){
				 printf("\nWelcome Admin, What would you like to do? \n");
				 isAdmin = true;
				 return "ADMIN";
			}
			else
				printf("incorrect password, please try again:\n");
		}
		BREAK

	CASE("CUSTOMER")
		return "CUSTOMER";
		BREAK
	DEFAULT
	std::cout << std::endl << "You entered an invalid option";
	return 0;
	END

	return 0;
}

/* * * * * * * * * * * * * * *
 *
 *
 * ADMIN Functions
 *
 *
 * * * * * * * * * * * * * * */

/* * * * * * *
 * Getters
 * * * * * * */

/**
 *  Returns list of all Entrees
 */
void getAllEntrees(){
	sqlite3 *db;
	int rc;
	//Open connection to database
	rc = sqlite3_open("MealPrepBizFinal.db", &db);

	//Output whether or db connection was successful
	if(rc){
		fprintf(stderr, "Can't open database: %s\n", sqlite3_errmsg(db));
	} else{

		//Prepare Select query to retrieve customer information from DB
		sqlite3_stmt *stmt;
		sqlite3_prepare_v2(db, "SELECT * FROM ENTREES;", -1, &stmt, NULL);

		//Get Query Results                                                                 /* 1 */
		//Loop Through query results and output each row
		printf("\n%-10s%-35s\n", "ID #", "Entree");
		while ( (rc = sqlite3_step(stmt)) == SQLITE_ROW) {                                              /* 2 */
			printf("-------------------------------------------\n");
			printf("%-10d%-35s\n",sqlite3_column_int(stmt, 0), sqlite3_column_text(stmt, 1));
		}
		//Close query
		sqlite3_finalize(stmt);
	}
	//close db connection
	sqlite3_close(db);

}

/**
 *  Returns a specific Entrees info
 *
 *  @param int entree_id: the id of the entree being searched for
 *
 *  @return string: the name of the entree
 */
string getEntree(int entree_id){
	const char* entree;
	string result;
	sqlite3 *db;
	int rc;

	//Open connection to database
	rc = sqlite3_open("MealPrepBizFinal.db", &db);

	//Output whether or db connection was successful
	if(rc){
		fprintf(stderr, "Can't open database: %s\n", sqlite3_errmsg(db));
	} else{

		//Prepare Select query to retrieve customer information from DB
		sqlite3_stmt *stmt;
		sqlite3_prepare_v2(db, "SELECT ENTREE FROM ENTREES WHERE ID = ?1;", -1, &stmt, NULL);

		//Bind the entree_id var value to the query
		if( rc == SQLITE_OK ) {
			sqlite3_bind_int(stmt, 1, entree_id);
		}
		//Get Query Results
		while ( (rc = sqlite3_step(stmt)) == SQLITE_ROW) {                                              /* 2 */
			entree = (char *)sqlite3_column_text(stmt,0);
			if(strlen(entree) > 3){
				//If entree exists, assign it to the result var and break the loop
				result = entree;

				break;
			}
		}
		//Close query
		sqlite3_finalize(stmt);
	}
	//close db connection
	sqlite3_close(db);

	return result;
}

/**
 *  Returns a specific Sides info
 *
 *  @param int side_id: the id of the side being searched for
 *
 *  @return string: the name of the side
 */
string getSide(int side_id){
	char* side;
	string result;
	sqlite3 *db;
	int rc;

	//Open connection to database
	rc = sqlite3_open("MealPrepBizFinal.db", &db);

	//Output whether or db connection was successful
	if(rc){
		fprintf(stderr, "Can't open database: %s\n", sqlite3_errmsg(db));
	} else{

		//Prepare Select query to retrieve customer information from DB
		sqlite3_stmt *stmt;
		sqlite3_prepare_v2(db, "SELECT SIDE FROM SIDES WHERE ID = ?1;", -1, &stmt, NULL);

		//Bind the side_id var value to the query
		if( rc == SQLITE_OK ) {
			sqlite3_bind_int(stmt, 1, side_id);
		}
		//Get Query Results
		while ( (rc = sqlite3_step(stmt)) == SQLITE_ROW) {                                              /* 2 */
			side = (char *)sqlite3_column_text(stmt,0);
			if(strlen(side) > 3){
				//If side exists, assign it to the result var and break the loop
				result = side;

				break;
			}
		}
		//Close query
		sqlite3_finalize(stmt);
	}
	//close db connection
	sqlite3_close(db);

	return result;
}

/**
 *  Returns list of all Sides
 */
void getAllSides(){
	sqlite3 *db;
	int rc;
	//Open connection to database
	rc = sqlite3_open("MealPrepBizFinal.db", &db);

	//Output whether or db connection was successful
	if(rc){
		fprintf(stderr, "Can't open database: %s\n", sqlite3_errmsg(db));
	} else{

		//Prepare Select query to retrieve customer information from DB
		sqlite3_stmt *stmt;
		sqlite3_prepare_v2(db, "SELECT * FROM SIDES;", -1, &stmt, NULL);

		//Loop Through query results and output each row
		printf("\n%-10s%-35s\n", "ID #", "SIDES");
		while ( (rc = sqlite3_step(stmt)) == SQLITE_ROW) {
			printf("-------------------------------------------\n");
			printf("%-10d%-35s\n",sqlite3_column_int(stmt, 0), sqlite3_column_text(stmt, 1));
		}
		//Close query
		sqlite3_finalize(stmt);
	}
	//close db connection
	sqlite3_close(db);
}
/**
 *  Returns list of all Orders
 */
void getAllOrders(){
	sqlite3 *db;
	int rc;
	rc = sqlite3_open("MealPrepBizFinal.db", &db);

	//Output whether or db connection was successful
	if(rc){
		fprintf(stderr, "Can't open database: %s\n", sqlite3_errmsg(db));
	} else{
		//Prepare Select query to retrieve customer information from DB
		sqlite3_stmt *stmt;
		rc = sqlite3_prepare_v2(db, "SELECT C.FNAME, C.LNAME, M.ENTREE, M.SIDE1,M.SIDE2, M.PRICE, P.DATE_ORDERED, P.AVAIL_PICKUP FROM MEALS M, CUSTOMERS C, PROCESSED P WHERE C.CUSTOMER_ID = P.CUSTOMER_ID AND M.ID = P.MEAL_ID ORDER BY C.FNAME ASC;", -1, &stmt, NULL);

		printf("\n%-15s%-15s%-35s%-20s%-20s%-10s%-20s%-20s\n","First Name", "Last Name", "Entree","Side 1", "Side 2", "Price", "Order Date", "Avail For Pickup");
		while ( (rc = sqlite3_step(stmt)) == SQLITE_ROW) {                                              /* 2 */
			printf("----------------------------------------------------------------------------------------------------------------------------------------------------------------\n");

			printf("%-15s%-15s%-35s%-20s%-20s$%-10s%-20s%-20s\n", sqlite3_column_text(stmt, 0), sqlite3_column_text(stmt, 1),sqlite3_column_text(stmt, 2),
					sqlite3_column_text(stmt, 3),sqlite3_column_text(stmt, 4),sqlite3_column_text(stmt, 5),sqlite3_column_text(stmt, 6),sqlite3_column_text(stmt, 7));
		}
		//Close query
		sqlite3_finalize(stmt);
	}
	//close db connection
	sqlite3_close(db);
}

/**
 *  Returns list of all Customers
 */
void getCustomers(){
	sqlite3 *db;

	    int rc;
	    //Open connection to database
	    rc = sqlite3_open("MealPrepBizFinal.db", &db);

	    //Output whether or db connection was successful
	    if(rc){
	        fprintf(stderr, "Can't open database: %s\n", sqlite3_errmsg(db));
	    } else{
	        //Prepare Select query to retrieve customer information from DB
	        sqlite3_stmt *stmt;
	        sqlite3_prepare_v2(db, "SELECT * FROM CUSTOMERS;", -1, &stmt, NULL);

	        //Get Query Results                                                                 /* 1 */
	        int i = 1;

	        //Loop Through query results and output each row
	        printf("\n%-5s%-15s%-15s%-15s%-20s\n","ID", "First Name", "Last Name", "Phone", "Address");
	        while ( (rc = sqlite3_step(stmt)) == SQLITE_ROW) {                                              /* 2 */
	        	printf("-----------------------------------------------------------------------------------------------\n");
	        	printf("%-5d%-15s%-15s%-15s%-20s\n", sqlite3_column_int(stmt, 0), sqlite3_column_text(stmt, 1),sqlite3_column_text(stmt, 2),sqlite3_column_text(stmt, 3),sqlite3_column_text(stmt, 4));
	        	i++;
	        }
	        //Close query
	       sqlite3_finalize(stmt);
	    }
	    //close db connection
	    sqlite3_close(db);
}

/* * * * * * *
 * Create
 * * * * * * */

/**
 *  Create a new entree
 *
 *  @param char entree: Name of the entree to be created
 */
void createEntree(char *entree){
	sqlite3* DB;
	char* messaggeError;
	char buffer[300];
	int insert = sqlite3_open("MealPrepBizFinal.db", &DB);

	sprintf(buffer,"INSERT INTO ENTREES (ENTREE) VALUES ('%s')", entree);

	insert = sqlite3_exec(DB, buffer, NULL, 0, &messaggeError);
	if (insert != SQLITE_OK) {
		std::cerr << "Error On INSERT!" << "\nSQL: " << buffer << "\n" << sqlite3_errmsg(DB) << std::endl;
		sqlite3_free(messaggeError);
	}
	else
		std::cout << "Record inserted Successfully!" << std::endl;

}

/**
 *  Create a new meal
 *
 *  @param string entree: Name of the entree selected
 *  @param string side1: Name of the first side selected
 *  @param string side2: Name of the second side selected
 *  @param int price: price of the meal
 */
void createMeal(string entree, string side1, string side2, int price){
	string result;
	sqlite3 *db;
	int rc;

	//Open connection to database
	rc = sqlite3_open("MealPrepBizFinal.db", &db);

	//Output whether or db connection was successful
	if(rc){
		fprintf(stderr, "Can't open database: %s\n", sqlite3_errmsg(db));
	} else{

		//Prepare Select query to retrieve customer information from DB
		sqlite3_stmt *stmt;
		sqlite3_prepare_v2(db,"INSERT INTO MEALS (ENTREE, SIDE1, SIDE2, PRICE) VALUES (?1, ?2, ?3, ?4);", -1, &stmt, NULL);
                                                                /* 1 */
		//bind argument values to the query
		if( rc == SQLITE_OK ) {
			sqlite3_bind_text(stmt, 1, entree.c_str(), -1, SQLITE_TRANSIENT);
			sqlite3_bind_text(stmt, 2, side1.c_str(), -1, SQLITE_TRANSIENT);
			sqlite3_bind_text(stmt, 3, side2.c_str(), -1, SQLITE_TRANSIENT);
			sqlite3_bind_int(stmt, 4, price);
		}

		rc = sqlite3_step(stmt);
		if (rc != SQLITE_DONE) {
		    printf("ERROR inserting data: %s\n", sqlite3_errmsg(db));
		} else
			std::cout << "\nRecord inserted Successfully!" << "\nEntree: " << entree << "\nSide: " << side1 << "\nSide: " << side2 << "\nPrice: $" << price <<  std::endl;

		//Close query
		sqlite3_finalize(stmt);
	}
	//close db connection
	sqlite3_close(db);
}
/**
 *  Create a new side
 *
 *  @param char side: Name of the side to be created
 */
void createSide(char *side){
	sqlite3* DB;
	char* messaggeError;
	char buffer[300];
	int insert = sqlite3_open("MealPrepBizFinal.db", &DB);

	sprintf(buffer,"INSERT INTO SIDES (SIDE) VALUES ('%s')", side);

	insert = sqlite3_exec(DB, buffer, NULL, 0, &messaggeError);
	if (insert != SQLITE_OK) {
		std::cerr << "Error On INSERT!" << "\nSQL: " << buffer << "\n" << sqlite3_errmsg(DB) << std::endl;
		sqlite3_free(messaggeError);
	}
	else
		std::cout << "Record inserted Successfully!" << std::endl;

}
/* * * * * * *
 * Update
 * * * * * * */

/**
 *  Update an entree. The user is shown and selects an option to update
 */
void updateEntree(){
	char entree_update[50];

	getAllEntrees();

	cout << "\nWhat is the ID of the Entree you would like to update? ";
	int entree_id;
	cin >> entree_id;

	cout << "\nEnter the updated Entree name: ";

	cin.get();
	cin.get(entree_update, 50);

	sqlite3* DB;
	char* messaggeError;
	char buffer[300];
	int insert = sqlite3_open("MealPrepBizFinal.db", &DB);

	sprintf(buffer,"UPDATE ENTREES SET ENTREE = '%s' WHERE ID = %d;", entree_update, entree_id);

	insert = sqlite3_exec(DB, buffer, NULL, 0, &messaggeError);
	if (insert != SQLITE_OK) {
		std::cerr << "Error On Update!" << "\nSQL: " << buffer << "\n" << sqlite3_errmsg(DB) << std::endl;
		sqlite3_free(messaggeError);
	}
	else
		std::cout << "Record Updated Successfully!" << std::endl;
}

/**
 *  Update a side. The user is shown and selects an option to update
 */
void updateSide(){
	char side_update[50];

	getAllSides();

	cout << "\nWhat is the ID of the Side you would like to update? ";
	int side_id;
	cin >> side_id;

	cout << "\nEnter the updated Side name: ";

	cin.get();
	cin.get(side_update, 50);

	sqlite3* DB;
	char* messaggeError;
	char buffer[300];
	int insert = sqlite3_open("MealPrepBizFinal.db", &DB);

	sprintf(buffer,"UPDATE SIDES SET SIDE = '%s' WHERE ID = %d;", side_update, side_id);

	insert = sqlite3_exec(DB, buffer, NULL, 0, &messaggeError);
	if (insert != SQLITE_OK) {
		std::cerr << "Error On Update!" << "\nSQL: " << buffer << "\n" << sqlite3_errmsg(DB) << std::endl;
		sqlite3_free(messaggeError);
	}
	else
		std::cout << "Record Updated Successfully!" << std::endl;
}
/* * * * * * * *
 * Delete
 * * * * * * */

/**
 *  Delete A Meal. The user is shown and selects an option to delete
 */
void deleteMeal(){

	getMeals();

	cout << "\nWhat is the ID of the Meal you would like to delete? ";
	int meal_id;
	cin >> meal_id;

	string confirm = "no";
	while(confirm == "no"){
		cout << "The meal you'd like to delete is: " << meal_id << " (yes/no)";
		cin >> confirm;
	}

	sqlite3* DB;
	char* messaggeError;
	char buffer[300];
	int exit = sqlite3_open("MealPrepBizFinal.db", &DB);

	sprintf(buffer,"DELETE FROM MEALS WHERE ID = %d", meal_id);
	exit = sqlite3_exec(DB, buffer, NULL, 0, &messaggeError);
	if (exit != SQLITE_OK) {
		std::cerr << "Error DELETE" << "\nSQL: " << buffer << std::endl;
	    sqlite3_free(messaggeError);
	}
	else
		std::cout << "Record deleted Successfully!" << std::endl;
}

/**
 *  Delete A Customer. The user is shown and selects an option to delete
 */
void deleteCustomer(int customerID){
	sqlite3* DB;
	char* messaggeError;
	char buffer[300];
	int exit = sqlite3_open("MealPrepBizFinal.db", &DB);

	sprintf(buffer,"DELETE FROM CUSTOMERS WHERE CUSTOMER_ID = %d", customerID);

	cout << "\n buffer: " << buffer << "\n";
	exit = sqlite3_exec(DB, buffer, NULL, 0, &messaggeError);
	if (exit != SQLITE_OK) {
		std::cerr << "Error DELETE" << "\nSQL: " << buffer << std::endl;
		sqlite3_free(messaggeError);
	}
	else
		std::cout << "Record deleted Successfully!" << std::endl;
}

/**
 *  Delete A Side. The user is shown and selects an option to delete
 */
void deleteSide(){

	getAllSides();

	cout << "\nWhat is the ID of the Side you would like to delete? ";
	int side_id;
	cin >> side_id;

	string confirm = "no";
	while(confirm == "no"){
		cout << "The meal you'd like to delete is: " << side_id << " (yes/no)";
		cin >> confirm;
	}

	sqlite3* DB;
	char* messaggeError;
	char buffer[300];
	int exit = sqlite3_open("MealPrepBizFinal.db", &DB);

	sprintf(buffer,"DELETE FROM SIDES WHERE ID = %d", side_id);

	exit = sqlite3_exec(DB, buffer, NULL, 0, &messaggeError);
	if (exit != SQLITE_OK) {
		std::cerr << "Error DELETE" << "\nSQL: " << buffer << std::endl;
		sqlite3_free(messaggeError);
	}
	else
		std::cout << "Record deleted Successfully!" << std::endl;
}

/**
 *  Delete An Entree. The user is shown and selects an option to delete
 */
void deleteEntree(){

	getAllEntrees();

	cout << "\nWhat is the ID of the Entree you would like to delete? ";
	int entree_id;
	cin >> entree_id;

	string confirm = "no";
	while(confirm == "no"){
		cout << "The order you'd like to delete is: " << entree_id << " (yes/no)";
		cin >> confirm;
	}
	sqlite3* DB;
	char* messaggeError;
	char buffer[300];
	int exit = sqlite3_open("MealPrepBizFinal.db", &DB);

	sprintf(buffer,"DELETE FROM ENTREES WHERE ID = %d", entree_id);

	exit = sqlite3_exec(DB, buffer, NULL, 0, &messaggeError);
	if (exit != SQLITE_OK) {
		std::cerr << "Error DELETE" << "\nSQL: " << buffer << "\n" << sqlite3_errmsg(DB) << std::endl;
		sqlite3_free(messaggeError);
	}
	else
		std::cout << "\nRecord deleted Successfully!" << std::endl;
}

/**
 *  Returns the option selected by the customer from the customer menu and executes that option
 *
 *  @param str res: the response of the customer
 *
 *  @return string: the response of the customer
 */
string customerSelected(string res){
	SWITCH (res)
		CASE("exit")
		cout << "Program Terminated...";
		return res;
	BREAK

	//View Menu
	CASE("1")
		getMeals();
		return res;
	BREAK

	DEFAULT
		std::cout << std::endl << "You entered an invalid option\n";
		return res;
	END

	return res;
}

/**
 *  Returns the option selected by the admin from the admin menu and executes that option
 *
 *  @param str res: the response of the admin
 *
 *  @return string: the response of the admin
 */
string adminSelected(string res){
	SWITCH (res)
			CASE("exit")
				cout << "Program Terminated...";
				return res;
			BREAK

			//View All Orders
			CASE("1")
				getAllOrders();
				return res;
			BREAK

			//View All Meals
			CASE("2")
				getMeals();
				return res;
			BREAK

			//Create New Meals
			CASE("3")
				int entree_id, side1_id, side2_id, price;
				string entree, side1, side2;

				//Show user the list of all entrees and get selected item
				getAllEntrees();
				cout << "\nEnter an Entree ID: ";
				cin >> entree_id;

				//Get name of entree from selected id
				entree = getEntree(entree_id);
				cout << "Entree Selected: " << entree << "\n";

				//Show user the list of all sides and get selected items
				getAllSides();
				cout << "\nEnter a Side ID: ";
				cin >> side1_id;
				cout << "Enter a 2nd Side ID: ";
				cin >> side2_id;

				//Get name of sides from selected ids
				side1 = getSide(side1_id);
				side2 = getSide(side2_id);

				cout << "Sides Selected: " << side1 << " & " << side2 << "\n";

				cout << "\nEnter a Price: ";
				cin >> price;

				//Create the new meal
				createMeal(entree, side1, side2, price);

				return res;
			BREAK

			//Create New Entrees
			CASE("4")
				char entree[50];

				cout << "What Entree would you like to add?";
				cin.get();
				cin.get(entree, 50);


				cout << "\nThe entree you would like to add is: " << entree << "\n";
				createEntree(entree);
				return res;
			BREAK

			//Create New Sides
			CASE("5")
				char side[50];

				cout << "What Side would you like to add?";
				cin.get();
				cin.get(side, 50);


				cout << "\nThe side you would like to add is: " << side << "\n";
				createSide(side);
				return res;
			BREAK

			//Delete Meals
			CASE("6")
				deleteMeal();
				return res;
			BREAK

			//Delete Entrees
			CASE("7")
				deleteEntree();
				return res;
			BREAK

			//Delete Sides
			CASE("8")
				deleteSide();
				return res;
			BREAK

			//View Customers Orders
			CASE("9")
				int cust_id;
				cout << "\nEnter Customer ID for order(s) Lookup: ";
				cin >>cust_id;

				getCustomersOrders(cust_id);
				return res;

			BREAK

			//Update Entrees
			CASE("10")
				updateEntree();
				return res;
			BREAK

			//Update Sides
			CASE("11")
				updateSide();
				return res;
			BREAK

			//View all customers
			CASE("12")
				getCustomers();
				return res;
			BREAK

			//View A Single Customer
			CASE("13")
				int id;
				char fname[20], lname[20];
				cout << "\nCustomer id (If you don't know, input 0): ";
				cin >> id;
				if(id > 1){
					getCustomer(id);
					return res;
				}
				else {
					cout << "\nCustomer's Fist name (IN CAPS):";
					cin >> fname;
					cout << "\nCustomer's Last name (IN CAPS):";
					cin >> lname;

					getCustomer(0,fname,lname);
					return res;
				}
			BREAK

			DEFAULT
				std::cout << std::endl << "You entered an invalid option\n";
			return res;
		END
		return res;
}
