//Library needed https://github.com/oklas/switch
	// After downloading and unzipping -> Right click the project -> properties
	// -> C/C++ Build -> Settings -> GCC C++ Compiler -> includes
	// add the include directory under include paths (-I)

/**** Admin Password: adminpass ****/
#include <iostream>
#include <switch>
#include "functions.h"
using namespace std;

int main(int argc, char* argv[]) {

	string res = "";

	//Prompt and get the type of user
	string user = getUser();

	SWITCH (user)

	//If admin, show the Admin menu
	CASE("ADMIN")

		while(res != "exit"){
			res = adminMenu();
			string ret = adminSelected(res);
		}
	BREAK

	//If customer, show the customer menu
	CASE("CUSTOMER")
		while(res != "exit"){
			res = customerMenu();
			string ret = customerSelected(res);
		}

	BREAK
	//If user mispells or gives another option.
	DEFAULT
		std::cout << std::endl << "You entered an invalid option";
		return 0;
	END

    return 0;
 }
